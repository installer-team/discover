Source: discover
Section: admin
Priority: optional
Maintainer: Debian Install System Team <debian-boot@lists.debian.org>
Uploaders: David Nusinow <dnusinow@debian.org>, Petter Reinholdtsen <pere@debian.org>
Build-Depends: debhelper-compat (= 13), libexpat1-dev, po-debconf, libusb-1.0-0-dev, autotools-dev, libtool-bin
Standards-Version: 3.8.0
Vcs-Git: https://salsa.debian.org/installer-team/discover.git
Vcs-Browser: https://salsa.debian.org/installer-team/discover

Package: discover
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, libdiscover2 (= ${binary:Version})
Suggests: lsb-base
Conflicts: discover1 (<< 2.0)
Description: hardware identification system
 Discover is a hardware identification system based on the libdiscover2
 library.  Discover provides a flexible interface that programs can
 use to report a wide range of information about the hardware that is
 installed on a Linux system.  In addition to reporting information,
 Discover includes support for doing hardware detection at boot time.

Package: libdiscover2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, discover-data
Conflicts: discover1 (<< 2.0), libdiscover1 (<< 2.0)
Description: hardware identification library
 libdiscover is a library enabling identification of various PCI,
 PCMCIA, and USB devices.

Package: libdiscover-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libdiscover2 (= ${binary:Version}), libexpat1-dev
Description: hardware identification library development files
 libdiscover is a library enabling identification of various PCI,
 PCMCIA, and USB devices.
 .
 This package contains the header files and static libraries needed for
 development.
