<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version='1.0'>

<!-- $Progeny$ -->

<!-- Skip index tags nested below the root level, in case we're
     XIncluding documents that have their own index. -->
<xsl:template match="index[../../..]"/>

</xsl:stylesheet>
