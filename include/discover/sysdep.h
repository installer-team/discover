/**
 * @file sysdep.h
 * @brief Private interface for system dependencies
 */

/* $Progeny$
 *
 * AUTHOR: John R. Daily <jdaily@progeny.com>
 *
 * Copyright 2002 Progeny Linux Systems, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef SYSDEP_H
#define SYSDEP_H

#ifdef __cplusplus
    extern "C" {
#endif

/**
 * Linked list node, used to return data specific to the
 * sysdep/device in question, to be passed to the user
 * under the "device" data path.
 */
typedef struct discover_sysdep_device_data {
    /** String giving the path for the data (not including "device" */
    char *path;
    /** String with the data's value */
    char *value;
    /** Pointer to the next data node */
    struct discover_sysdep_device_data *next;
} discover_sysdep_device_data_t;

/**
 * Linked list node, used to return hardware-identification
 * information discovered by the system-dependent interface
 */
typedef struct discover_sysdep_data {
    /** String describing the general device class */
    char *busclass;
    /** String describing the manufacturer or distributor */
    char *vendor;
    /** String identifying the model */
    char *model;
    /** Pointer to any sysdep/device specific data to be returned */
    discover_sysdep_device_data_t *data;
    /** Pointer to the next hardware device discovered */
    struct discover_sysdep_data *next;
} discover_sysdep_data_t;

discover_sysdep_data_t *_discover_sysdep_data_new(void);
discover_sysdep_device_data_t *_discover_sysdep_device_data_new(void);
void _discover_free_sysdep_data(discover_sysdep_data_t *);
void _discover_free_sysdep_device_data(discover_sysdep_device_data_t *);

discover_sysdep_data_t *_discover_get_ata_raw(void);
discover_sysdep_data_t *_discover_get_pci_raw(void);
discover_sysdep_data_t *_discover_get_usb_raw(void);
discover_sysdep_data_t *_discover_get_pcmcia_raw(void);
discover_sysdep_data_t *_discover_get_scsi_raw(void);

#ifdef __cplusplus
    }
#endif

#endif

/*
 * Local variables:
 * c-file-style: "progeny"
 * indent-tabs-mode: nil
 * End:
 */
/* vim: set cin fo=tcroq sw=4 et sts=4 tw=75: */
