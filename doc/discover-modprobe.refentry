<!--
Copyright 2002 Hewlett-Packard Company

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
-->

  <refentryinfo>
    <releaseinfo>$Progeny$</releaseinfo>

    <authorgroup>
      &author.jdaily;
    </authorgroup>
    <copyright>
      <year>2002</year>
      <holder>&progeny.inc;</holder>
    </copyright>
  </refentryinfo>

  <refmeta>
    <refentrytitle>discover-modprobe</refentrytitle>
    <manvolnum>8</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>discover-modprobe</refname>
    <refpurpose>kernel module loading using discover(1)</refpurpose>
  </refnamediv>

  <refsynopsisdiv>

    <cmdsynopsis>
      <command>discover-modprobe</command>
      <arg choice="opt">-n</arg>
      <arg choice="opt">-v</arg>
    </cmdsynopsis>

  </refsynopsisdiv>


  <refsect1 id="dm8-rs-description">
    <title id="dm8-rs-title-description">Description</title>

    <para><command>discover-modprobe</command> loads kernel modules
    identified by &discover-command;. It will typically be invoked
    automatically at boot time.</para>

  </refsect1>

  <refsect1 id="dm8-rs-options">
    <title id="dm8-rs-title-options">Options</title>

    <variablelist>
      <varlistentry>
        <term><option>-n</option></term>
        <listitem>
          <para>Echo the <command>modprobe</command> invocations instead of
          running them.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-v</option></term>
        <listitem>
          <para>Be verbose.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>

  <refsect1 id="dm8-rs-files">
    <title id="dm8-rs-title-files">Files</title>

    <variablelist>
      <varlistentry>
        <term><filename>&sysconfdir;/discover-modprobe.conf</filename></term>
        <listitem>
          <para>This configuration file defines the types of modules
          to load by default, and specific modules
          <emphasis>not</emphasis> to load.</para>
        </listitem>
      </varlistentry>

      &crash-snippet;
    </variablelist>

  </refsect1>


  <refsect1 id="dm8-rs-see-also">
    <title id="dm8-rs-title-see-also">See Also</title>

    <para>discover-modprobe.conf(5), modprobe(8), discover(1)</para>

  </refsect1>

<!-- Local variables: -->
<!-- mode: xml -->
<!-- sgml-parent-document: ("guide.xml" "refentry" "refentry") -->
<!-- End: -->
<!-- vim: set ai et sts=2 sw=2 tw=65: -->
